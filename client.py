#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import asyncio
import logging
import time

from wotpy.wot.servient import Servient
from wotpy.wot.wot import WoT

logging.basicConfig()
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


async def main():
    wot = WoT(servient=Servient())
    # consumed_thing = await wot.consume_from_url('http://localhost:8080/teros-sensor')
    # consumed_thing = await wot.consume_from_url('https://teros.vaimee.com:9090/teros-sensor') 
    consumed_thing = await wot.consume_from_url('http://127.0.0.1:9092/teros-sensor')

    LOGGER.info('#### Consumed Thing: {}'.format(consumed_thing))

    statusmqtt = await consumed_thing.read_property('mqttStatus')
    LOGGER.info('MQTT STATUS: {}'.format(statusmqtt))

    depth_sensors = []
    depth_sensors.append({
          "name":"sensore_1",
          "depth":0.25
    })
    depth_sensors.append({
          "name":"sensore_2",
          "depth":0.5
    })
    depth_sensors.append({
          "name":"sensore_3",
          "depth":1
    })
    await consumed_thing.write_property('humidity_sensors_conf', depth_sensors)
    set_depth_check = await consumed_thing.read_property('humidity_sensors_conf')
    LOGGER.info('set depth sensors, value is: {}'.format(set_depth_check))

    soil_moisture = await consumed_thing.read_property('soil_moisture')
    LOGGER.info('test value soil_moisture, value is: {}'.format(soil_moisture))

    statusmqtt = await consumed_thing.invoke_action('mqttConnect')
    LOGGER.info('MQTT CONNECT: {}'.format(statusmqtt))


    LOGGER.info('#### Start mqtt subscrber...')
    statusmqtt = await consumed_thing.read_property('mqttStatus')
    LOGGER.info('MQTT STATUS: {}'.format(statusmqtt))

    LOGGER.info('#### Sleep 5s')
    time.sleep(5)

    statusmqtt = await consumed_thing.read_property('mqttStatus')
    LOGGER.info('MQTT STATUS: {}'.format(statusmqtt))

    LOGGER.info('#### Sleep 80s')
    time.sleep(80)

    LOGGER.info('#### Readings values')

    soil_moisture = await consumed_thing.read_property('soil_moisture')
    LOGGER.info('soil_moisture value is: {}'.format(soil_moisture))


    # statusmqtt = await consumed_thing.invoke_action('mqttDisconnect')
    # LOGGER.info('MQTT DISCONNECT: {}'.format(statusmqtt))

    # while statusmqtt:
    #     LOGGER.info('#### Sleep 5s')
    #     time.sleep(5)
    #     statusmqtt = await consumed_thing.read_property('mqttStatus')
    #     LOGGER.info('MQTT STATUS: {}'.format(statusmqtt))
    
    LOGGER.info('#### Test ends')

if __name__ == '__main__':
    loop = asyncio.run(main())
