# VoTeros

This project contains the code and implementation for the representation of the Meter Teros sensor within the Web of Things (WoT) framework using wotpy. The goal of VoTeros is to provide a comprehensive and scalable solution for integrating Meter Teros sensors into WoT environments.

[Here you can find a short description of the bash scripts that will help you to use and test this Vo](./docs/scripts.rst)

This Virtual Object (Vo) can be used directly on your localhost or with Docker. We recommend using the Docker version, as running the code directly on your operating system, on Windows-based systems, can encounter difficulties.


# Install

### Prerequisites:

- Python
- Docker


### Steps

All the scripts must to be runned by the rot folder of this repository.

- Run the script [init.sh](./scripts/init.sh)  
    ```
    ./scripts/init.sh
    ```
- Install Docker if you don't have it, and run [runDocInflux.sh](./scripts/runDocInflux.sh)  
    ```
    ./scripts/runDocInflux.sh
    ```
- Configure your .env file
    - create the .env file in the root folder of this repo and set the following variables:
        - influx access and endpoint
            - influx_address="http://localhost:8086"
            - influx_dbUser=my-username
            - influx_dbPass=my-password
            - influx_dbToken=my-token
        - MQTT broker for the Meter Teros sensor
            - MQTT_TOPIC="..."
            - MQTT_HOST="mqtt://..."
- Start the Vo, run the script [runVoTeros.sh](./scripts/runVoTeros.sh)  
    ```
    ./scripts/runVoTeros.sh
    ```


# Docker
- Setup your configuration editing the file [vo_teros_draft.yaml](./vo_teros_draft.yaml)
    - edit the property bindingNB.hostname with your domain or "localhost"
    - if you are running your docker locally or you want to manage your SSL certificate directly from the Vo remove the property bindingNB.externalCertificate. Keep that property ONLY if you run this Docker version of the Vo delegating the SSL certificate to an upper layer like "traefik".
- Configure your .env file
    - create the .env file in the root folder of this repo and set the following variables:
        - influx access and endpoint
            - influx_address="http://localhost:8086"
            - influx_dbUser=my-username
            - influx_dbPass=my-password
            - influx_dbToken=my-token
        - MQTT broker for the Meter Teros sensor
            - MQTT_TOPIC="..."
            - MQTT_HOST="mqtt://..."
- Build your docker image running the script [dockerbuild.yaml](./scripts/dockerbuild.sh)  
    ```
    ./scripts/dockerbuild.sh
    ```
- Run the docker image running the script [dockerbuild.yaml](./scripts/dockerrun.sh)  
    ```
    ./scripts/dockerrun.sh
    ```

# Testing
Note: if you encounter problem running the script "startmock" on Windows OS you can create a docker container based on UNIX for run that script.
- Start the mock data producer, run the script [startmock.sh](./scripts/startmock.sh)  
    ```
    ./scripts/startmock.sh
    ```
- Start the python client tester, run the script [runTestClient.sh](./scripts/runTestClient.sh)  
    ```
    ./scripts/runTestClient.sh
    ```
- Wait until the end of the test (more or less 3min)
- You can now login in to your influxDB instance, and find all the data that the Vo collected.

