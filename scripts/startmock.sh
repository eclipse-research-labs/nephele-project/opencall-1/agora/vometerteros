#!/bin/bash

# Check Mosquitto
if ! systemctl is-active --quiet mosquitto; then
    echo "Mosquitto is not running. Attempting to start..."
    sudo systemctl start mosquitto
    
    if ! systemctl is-active --quiet mosquitto; then
        echo "Error: Mosquitto failed to start."
        exit 1
    fi
fi

echo "Mosquitto running."

while true; do
    battery=$(shuf -i 1-100 -n 1)
    dutycycle_min=$(shuf -i 5-15 -n 1)

    conductivity_1=$(shuf -i 1-100 -n 1)
    permittivity_1=$(echo "scale=1; $(shuf -i 100-1000 -n 1) / 10" | bc)
    temperature_1=$(echo "scale=1; $(shuf -i 100-400 -n 1) / 10" | bc)
    water_content_1=0$(echo "scale=2; $(shuf -i 1-100 -n 1) / 100" | bc)

    conductivity_2=$(shuf -i 1-100 -n 1)
    permittivity_2=$(echo "scale=1; $(shuf -i 100-1000 -n 1) / 10" | bc)
    temperature_2=$(echo "scale=1; $(shuf -i 100-400 -n 1) / 10" | bc)
    water_content_2=0$(echo "scale=2; $(shuf -i 1-100 -n 1) / 100" | bc)

    conductivity_3=$(shuf -i 1-100 -n 1)
    permittivity_3=$(echo "scale=1; $(shuf -i 100-1000 -n 1) / 10" | bc)
    temperature_3=$(echo "scale=1; $(shuf -i 100-400 -n 1) / 10" | bc)
    water_content_3=0$(echo "scale=2; $(shuf -i 1-100 -n 1) / 100" | bc)

    conductivity_6=$(shuf -i 1-100 -n 1)
    freezing_flag=$(shuf -i 0-1 -n 1)
    temperature_6=$(echo "scale=1; $(shuf -i 100-400 -n 1) / 10" | bc)
    water_depth=$(shuf -i 1-10 -n 1)

    json_payload=$(cat <<EOF
{
    "object": {
        "Battery": $battery,
        "Dutycycle_min": $dutycycle_min,
        "sensore_1": {
            "Conductivity": $conductivity_1,
            "Permittivity": $permittivity_1,
            "Temperature": $temperature_1,
            "WaterContent": $water_content_1
        },
        "sensore_2": {
            "Conductivity": $conductivity_2,
            "Permittivity": $permittivity_2,
            "Temperature": $temperature_2,
            "WaterContent": $water_content_2
        },
        "sensore_3": {
            "Conductivity": $conductivity_3,
            "Permittivity": $permittivity_3,
            "Temperature": $temperature_3,
            "WaterContent": $water_content_3
        },
        "sensore_6": {
            "Conductivity": $conductivity_6,
            "FreezingFlag": $freezing_flag,
            "Temperature": $temperature_6,
            "WaterDepth": $water_depth
        }
    }
}
EOF
)

    mosquitto_pub -t "test/topic" -m "$json_payload"
    echo "Published: $json_payload"
    sleep 60
done
