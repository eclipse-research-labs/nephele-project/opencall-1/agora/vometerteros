#!/bin/sh
echo "Setting vo inlfaxDB yaml credential"
echo "" >> /app/vo_teros.yaml
echo "    address: ${influx_address}" >> /app/vo_teros.yaml
echo "    dbUser: ${influx_dbUser}" >> /app/vo_teros.yaml
echo "    dbPass: ${influx_dbPass}" >> /app/vo_teros.yaml
echo "    dbToken: ${influx_dbToken}" >> /app/vo_teros.yaml
cat /app/vo_teros.yaml
echo "Start Vo"
vo-wot -t teros_td.json -f vo_teros.yaml teros.py