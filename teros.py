#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
WoT application to expose a Thing that provides the values of Meter Teros Sensor
"""
from dotenv import load_dotenv
import os
import asyncio
import json
from wotpy.wot.exposed.thing import ExposedThing
from wotpy.wot.servient import Servient
from amqtt.client import MQTTClient, ClientException
from amqtt.mqtt.constants import QOS_0, QOS_1, QOS_2

### LOGGING
import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
amqtt_logger = logging.getLogger("amqtt")
amqtt_logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
amqtt_logger.addHandler(handler)

### MQTT 
load_dotenv()
MQTT_TOPIC = os.environ.get('MQTT_TOPIC')
MQTT_HOST= os.environ.get('MQTT_HOST')
logging.info(f"connectting to: {MQTT_HOST}")
logging.info(f"subscribing to: {MQTT_TOPIC}")
mqttRun=False
mqttStatus_init=False
### DATA
duty_cycle=None
battery=None
depth_sensors=0
humidity_sensors=0
humidity_sensors_conf=[]
soil_moisture=[]
humidity_sensors_values=[]
depth_sensors_values=[]

async def mqtt_subscriber():
    global mqttRun
    mqttRun=True
    client = MQTTClient()
    await client.connect(MQTT_HOST,True)   
    await client.subscribe([
        (MQTT_TOPIC, QOS_0)
    ])
    try:
        while mqttRun:
            message = await client.deliver_message()
            packet = message.publish_packet
            msg = packet.payload.data.decode('utf-8')
            # print(f"recived msg: {msg}")
            json_object = json.loads(msg)
            if json_object['object'] is not None:
                json_object = json_object['object']
            _soil_moisture=[]
            _depth_sensors=[]
            _humidity_sensors=[]
            _humidity_sensors_conf = await exposed_thing.read_property('humidity_sensors_conf')                   
            # print(f"_humidity_sensors_conf {_humidity_sensors_conf}")
            _hashmap_sensors = {}
            for el in _humidity_sensors_conf:
                _hashmap_sensors[el["name"]]= el["depth"]
            for key, value in json_object.items():
                if key == "Battery":
                    await exposed_thing.properties['battery'].write(value)
                elif key == "Dutycycle_min":
                    await exposed_thing.properties['duty_cycle'].write(value)
                else: 
                    # devide the 2 possible kide of sensor values                                
                    # print(f"value {value}")
                    if 'WaterContent' in value:
                        if not ('Conductivity' in value) or not ('Permittivity' in value) or not ('Temperature' in value):                            
                            print(f"Warning received {key}, that has not a valid sensor output")
                        else:
                            if key in _hashmap_sensors:
                                print(f"Warning received  sensor name {value} as a humidity sensor, that is not present in the configuration")
                        
                            _soil_moisture.append({
                                "depth":_hashmap_sensors[key],
                                "value":value['WaterContent']
                            })
                            _humidity_sensors.append({
                                "conductivity":value['Conductivity'],
                                "permittivity":value['Permittivity'],
                                "temperature":value['Temperature'],
                                "water_content":value['WaterContent'],
                                "depth":_hashmap_sensors[key]
                            })
                    elif 'WaterDepth' in value:
                        if not ('Conductivity' in value) or not ('FreezingFlag' in value) or not ('Temperature' in value):                            
                            print(f"Warning received {key}, that has not a valid sensor output")
                        else:
                            _depth_sensors.append({
                                "conductivity":value['Conductivity'],
                                "freezing_flag":value['FreezingFlag'],
                                "temperature":value['Temperature'],
                                "water_depth":value['WaterDepth']
                            })
                    else:
                        print(f"Warning received {key}, that is not a valid sensor")
                
            if len(_depth_sensors)>0:                 
                await exposed_thing.properties['depth_sensors'].write(len(_depth_sensors))
                await exposed_thing.properties['depth_sensors_values'].write(_depth_sensors)
            else:
                print(f"No depth sensors recevid, keeping old values")

            if len(_humidity_sensors)>0:                 
                await exposed_thing.properties['humidity_sensors'].write(len(_humidity_sensors))
                await exposed_thing.properties['humidity_sensors_values'].write(_humidity_sensors)
                await exposed_thing.properties['soil_moisture'].write(_soil_moisture)
            else:
                print(f"No humidity sensors recevid, keeping old values")             
            print("Parsing end")
    except ClientException as ce:
        print(f"Client exception: {ce}")
    finally:
        await client.disconnect()
        await exposed_thing.properties['mqttStatus'].write(False)

async def on_mqtt_message(payload):
        print(f"Messaggio MQTT ricevuto: {payload}")


async def mqttConnect_handler(params):
    if not await exposed_thing.read_property('mqttStatus'):
        #  mqtt_subscriber is async but we will not await it on purpose
        await exposed_thing.properties['mqttStatus'].write(True)
        asyncio.create_task(mqtt_subscriber())
        current_connect_status = await exposed_thing.read_property('mqttStatus')
        print(f"subscribing status: {current_connect_status}")
        # await terosrawSubcribe()
    return current_connect_status

async def mqttDisconnect_handler(params):
    global mqttRun
    mqttRun=False
    return await exposed_thing.read_property('mqttStatus')