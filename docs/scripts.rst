Scripts
====

This section describes the scripts contained in the `folder "scripts" <../scripts>`_

To run the scripts correctly, your position should be the root folder of this repository.
-------------

================  ==============================================
Script             Description
================  ==============================================
init.sh           In order to install all the necessary dependencies.
runVoTeros.sh      In order to run the Vo.
runTestClient.sh  In order to run the test client.
runDocInflux.sh   In order to run the Dockerized influx.
populateyaml.sh   Used by the doker image to set the influx credential and run the Vo.
dockerbuild.sh    Build the docker image from the dokerfile
dockerrun.sh      Run the docker image (remember to fill your .env before)
startmock.sh      Run a mqtt producer for mock data
================  ==============================================

