FROM python:3.11-slim
WORKDIR /app

RUN pip install vo-wot
RUN pip install python-dotenv

COPY teros.py /app/teros.py
COPY teros_td.json /app/teros_td.json
COPY vo_teros_draft.yaml /app/vo_teros_draft.yaml
#COPY scripts/populateyaml.sh /app/populateyaml.sh
#RUN chmod +x /app/populateyaml.sh

# Copy configurator
COPY populate_yaml.py /app/populate_yaml.py
COPY docker_cmd.sh /app/docker_cmd.sh
RUN chmod +x /app/docker_cmd.sh

CMD ["/app/docker_cmd.sh"]