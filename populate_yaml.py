# ONLY USED IN DOCKER CONTAINER
import os
import yaml
from dotenv import load_dotenv


def read_env_vars():
    # Read environment variables from .env file
    env_vars = {key: value for key, value in os.environ.items()}
    return env_vars

def modify_yaml_file(yaml_file_path, env_vars):
    # Read the original YAML file
    with open(yaml_file_path, 'r') as file:
        yaml_content = yaml.safe_load(file)

    # Modify the YAML content based on environment variables
    #Host
    yaml_content["bindingNB"]["hostname"]= env_vars["host"]
    #PORTS
    yaml_content["catalogue"]= int(env_vars["catalogue_port"])
    yaml_content["bindingNB"]["ports"]["httpPort"]= int(env_vars["http_port"])
    #Cert
    if env_vars["externalCert"]==("true" or "True"):
        yaml_content["bindingNB"]["externalCertificate"]= True
    else:
        yaml_content["bindingNB"]["externalCertificate"]= False
    #Influx
    yaml_content["databaseConfig"]["timeseriesDB"]["address"]= env_vars["influx_address"]
    yaml_content["databaseConfig"]["timeseriesDB"]["dbUser"]= env_vars["influx_dbUser"]
    yaml_content["databaseConfig"]["timeseriesDB"]["dbPass"]= env_vars["influx_dbPass"]
    yaml_content["databaseConfig"]["timeseriesDB"]["dbToken"]= env_vars["influx_dbToken"]

    # Write the modified content back to the YAML file
    with open(yaml_file_path, 'w') as file:
        yaml.safe_dump(yaml_content, file)

    # Print file contents to screen
    with open(yaml_file_path, 'r') as file:
        print(file.read())

load_dotenv("./.env")
env_vars = read_env_vars()

# Modify the YAML file
modify_yaml_file("./vo_teros_draft.yaml", env_vars)